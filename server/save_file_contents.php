<?php

if(!isset($_POST['contents']) || !isset($_POST['file'])){

	die("Please open a file before saving!");
}

$filename=$_POST['file'];
$contents=$_POST['contents'];



if(is_file($filename)){
    
    
    file_put_contents($filename, $contents);
    echo "File Saved Successfully.";
    
}
else{
    
    echo "File doesn't exist!";
    
}
