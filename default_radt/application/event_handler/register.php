<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *default_eventhandler
 *
 * @author silva
 */
class handler_register extends register{
    
    public function button_one__onclick(){
        $component=getComponent('status');
        $component->setHTML("<b>Text Change Successfull!</b>");
    }
    

public function login__onmousedown()
{ 
    radt_component::redirect("login");
 }

public function Register__onclick()
{ 
    $s=getComponent('status');
    $u=getComponent('username')->getValue();
    $p1=getComponent('p1')->getValue();
    $p2=getComponent('p2')->getValue();
    if($p1!=$p2 || $u=="" || $p1==""){
        
        $s->setHTML("<font color=red>Invalid Data.</font>");
        return;
    }
    
    
    $user=User::find_by_username($u);
    
    if($user!=null){
          $s->setHTML("<font color=red>user already exists.</font>"); 
          return;
    }
    
    
    $user=new User();
    $user->username=$u;
    $user->password=$p1;
    
    $user->save();
    $s->setHTML("<font color=green>Registration Successfull.</font>");   
    
 }
}