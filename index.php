<?php
require "auth.php";
require "header.php";
require "lib.dir.php";
?>

<body>

<div class="head">
<img src="images/head.jpg" height="40px" width="100%"><img src="images/logo.png" class="logo" ></img>
</div>


<div class="top_container">
<div class="project_creator_name">hello <?php  echo $_SESSION['username'];  ?></div>
<div class="logout">
<a  href="logout.php">Logout</a></div>



<a href="#" onmousedown="show_create_prj_form()" ><img src="images/create_project_button.png" class="create_project_button"/></a> 
<div class="create_project_form" id="create_project_form">
<b>Project Name:</b>
<input type="text" name="project_name" id='project_name' />
<input type="submit" value="Create Project!" onmousedown='create_project()'/>

</div>


<script type="text/javascript">
$('#create_project_form').hide();
//------------------------------------
function show_create_prj_form(){
$('#create_project_form').show();

}
function confirm_delete(project)
{
    var con_del=confirm("Are you sure?");
    
    if(con_del==true)
        {
         window.location='server/delete_project.php?project='+project; 
            
        }
    
}


//------------------------------
function create_project(){

var prj_name=$('#project_name').val();
//alert(prj_name);




$.post("server/project.php?do=create_project", {
                 project_name: prj_name

            } , function(data){
                alert(data.message);
				
				var i=0;
				var str="";
				
				
				
				for(i=0;i<data.projects.length;i++){
				
				
				str=str+"<tr class='row'><td width=90%>"+data.projects[i]+"</td><td width=10%><a href='editor.php?project="+data.projects[i]+"'><img src='images/edit_button.png' height='30px' width='30px' title='Edit'/></a></td><td><a  href='#' onclick=\"confirm_delete('"+data.projects[i]+"')\"><img src='images/delete_button.png'  height='30px' width='30px' title='Delete'/></a></td></tr><tr><td class='separator'></td></tr>";
				 // $.('#list_projects').append(str);
				
				}
				$('#list_projects').html(str);
				$('#create_project_form').hide();
			
            },'json');

}




</script>
<table class="project_table_cover">
    <tr>
        <td>
<table  id="list_projects" class="project_table">
<?php
$projects=get_projects();


foreach($projects as $project){
echo "<tr class='row'><td width=90%>";

echo $project;

echo "</td><td width=10%><a href='editor.php?project=".$project."'><img src='images/edit_button.png' height='30px' width='30px' title='Edit'/></a></td>
<td><img src='images/delete_button.png'  onclick=\"confirm_delete('$project')\" height='30px' width='30px' title='Delete'/></td>

</tr><tr><td class='separator'></td></tr>";
}

?>

</table>
            </td>
            </tr>
<tr height="200px">
    <td>
        
    </td>
</tr>
<tr>
    <td>
    <div style="position:relative;background:black;height:30px;width:100%"></div>
    </tr>
    </td>
</table>
    

<?php
require "footer.php";
?>