<?php



class view{



 public $view_vars = array();
  
  
	function __construct(){
	
	
	}
//--------------------------------------------------------------------------------------------------
  public function assign($key, $value=null)
  {
    if(isset($value))
      $this->view_vars[$key] = $value;
    else
      foreach($key as $k => $v)
        if(is_int($k))
          $this->view_vars[] = $v;
        else
          $this->view_vars[$k] = $v;
  }  
 //---------------------------------------------------------------------------------------------------- 
  public function display($filename,$view_vars=null)
  {
    return $this->_view("application/view/"."{$filename}.php",$view_vars);
  }  

//------------------------------------------------------------------------------------------------------  
  public function fetch($filename,$view_vars=null)
  {
    ob_start();
    $this->display($filename,$view_vars);
    $results = ob_get_contents();
    ob_end_clean();
    return $results;
  }  

//----------------------------------------------------------------------------------------------------------
  public function _view($filepath,$view_vars = null)
  {
    if(!file_exists($filepath))
      throw new Exception("Unable to load view, Unknown file '$filepath' ");

    // bring view vars into view scope
      extract($this->view_vars);
    if(isset($view_vars))
      extract($view_vars);
    include($filepath);
  }

}