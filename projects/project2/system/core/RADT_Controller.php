<?php


class RADT_Controller{

public $html;
	function __construct(){
	
		global $HTML;
		$this->library('view');
		$this->html=&$HTML;
		
	}
//------------------------------------------------------------------------------------------------
	function library($library){
	
		if(is_file("system/libraries/".$library.".php")){
		
			require "system/libraries/".$library.".php";
			
			$this->$library=new $library();
			
		
		
		}
		else{
		
			fatal_error("library `$library` could not be loaded.");
		
		}
	
	}
//-------------------------------------------------------------------------------------------------
	function load_model($model){
	
	
		if(is_file("application/model/".$model.".php")){
		
			require_once "application/model/".$model.".php";
				//var_dump($this);
			//var_dump($model);
                        
                        if(strpos($model,"/")){
                            $models=explode("/",$model);
                            $model=$models[count($models)-1];
                        }
                        
			$modelx="model_".$model;
			$this->$model=new $modelx();
			
			//var_dump($model);		
		
		}
		else{
		
			fatal_error("model `$model` could not be loaded.");
		
		}
	
	
	}
//-------------------------------------------------------------------------------------------------
	function load_html($str){

	global $BASE_PATH;
		$str=str_replace("BASE_PATH/",$BASE_PATH,$str);
		$this->html=str_get_html($str);
	
	}
//------------------------------------------------------------------------------------------------
	function display_html(){
	
		$script="<script src='".base_path()."/static/jquery.js'></script>";
		echo $this->html->save().$script.$this->get_event_code();
	
	}
//------------------------------------------------------------------------------------------------
	function get_event_code(){
		
		global $EVENT_JSCODE;
		return $EVENT_JSCODE;
	
	}

}