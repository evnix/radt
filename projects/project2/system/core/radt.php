<?php

if (!defined('RADF'))
    exit('No direct script access allowed');
require_once 'system/libraries/php-activerecord/ActiveRecord.php';
$HTML = null;

function d_buglog($val) {

    global $RADF_DEBUG;
    if ($RADF_DEBUG == true) {
        echo $val;
    }
}

$EVENT_JSCODE = "";
$EVENT_EVAL_CODE = "";
$radt_components = array();

function sel($id) {
    global $HTML;
    return $HTML->getElementById($id);
}

function curPageURL() {
    $pageURL = 'http';
// if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function base_path($str=null) {

            global $BASE_PATH;
    if ($str === null) {

        return $BASE_PATH;
    }
    
    if($str[0]!='/'){
        $str="/".$str;
    }
    
    return $BASE_PATH.$str;
    
}

function fatal_error($mesg) {

    //print("<br/><b>Fatal Error: </b>".$mesg);

    throw new Exception("RADT ERROR: " . $mesg);
}

class radt {

    public $args = array(); //ARGUMENTS PASSED ON REQUEST
    public $controller_path = "application/controller/notfound.php"; //absolute path to the controller
    public $controller = "notfound"; //default controller
    public $controller_obj = null; //the controller object
    public $events_exist = false;
    public $inc = -1; // just for increment.

    //-------------------------------------

    function __construct() {

        $this->get_args();
        $this->controller_check();
    }

//-----------------------------------------
    function get_args() {

        global $script_bits;
        global $request_bits;

        $i = 0;

        foreach ($request_bits as $rbit) {

            if (isset($script_bits[$i])) {

                if ($request_bits[$i] == $script_bits[$i]) {
                    
                } else {

                    if ($request_bits[$i] != "")
                        $this->args[] = $request_bits[$i];
                }
            }
            else {

                if ($request_bits[$i] != "") {

                    $this->args[] = $request_bits[$i];
                }
            }

            $i++;
        }
        //var_dump($this->args);
    }

//-----------------------------------------------------------------------------------------------------------------
    function controller_check($path="application/controller/") {

        //var_dump($path);
        $this->inc++;
        if (!isset($this->args[0])) {

            $this->controller_path = "application/controller/radf_default.php";
            $this->controller = "radf_default";
            return;
        }

        if (!isset($this->args[$this->inc])) {

            return;
        }

        if (is_file($path . $this->args[$this->inc] . ".php")) {

            $this->controller_path = $path . $this->args[$this->inc] . ".php";
            $this->controller = $this->args[$this->inc];

            if (is_file(str_replace("/controller/", "/event_handler/", $path . $this->args[$this->inc] . ".php"))) {

                $this->events_exist = true;
                $this->handler_path = str_replace("/controller/", "/event_handler/", $path) . $this->args[$this->inc] . ".php";
                $this->handler = "handler_" . $this->args[$this->inc];
            }

            return;
        }

        if (is_dir($path . $this->args[$this->inc])) {


            $this->controller_check($path . $this->args[$this->inc] . "/");
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    function run_controller() {

        require "component.php";
        require "system/core/RADT_Controller.php";
        require "system/core/eventHandler.php";
        require "system/core/model.php";

        if (is_file($this->controller_path)) {
            require $this->controller_path;
        } else {
            fatal_error("cannot find controller file: " . $this->controller_path . " .");
        }
        if ($this->events_exist == true) {

            require $this->handler_path;
            $this->controller_obj = new $this->handler();
            $eventHandler = new eventHandler($this->controller_path);
            $eventHandler->get_methods($this->controller_obj);
            $eventHandler->generate_client_code();
            global $EVENT_JSCODE;
            $EVENT_JSCODE = $eventHandler->get_code();
        } else {
            if (class_exists($this->controller)) {
                $this->controller_obj = new $this->controller();
            } else {
                fatal_error("Controller class " . $this->controller . " not defined.");
            }
        }


        $this->inc++;



        if (isset($this->args[$this->inc])) {

            //check if the request is a event or not.
            $isevent = false;
            $methods = $eventHandler->registered_methods;
            //echo 'is eventx';
            //var_dump($this->args);
            //var_dump($methods);
            foreach ($methods as $method) {

                if ($method["name"] == $this->args[$this->inc]) {

                    $isevent = true;
                    break;
                    //echo 'is event';
                }
            }

            if (method_exists($this->controller_obj, $this->args[$this->inc])) {

                call_user_func_array(array($this->controller_obj, $this->args[$this->inc]), array_slice($this->args, $this->inc + 1));
                if ($isevent == true) {
                    global $EVENT_EVAL_CODE;
                    echo $EVENT_EVAL_CODE;
                }
            } else {

                fatal_error("method `" . $this->args[$this->inc] . "` not found.");
            }
        } else {

            if (method_exists($this->controller, 'index')) {

                call_user_func_array(array($this->controller_obj, 'index'), array());
            } else {

                fatal_error("Index method not found.");
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------
}
