

<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 *  abc
 *
 * @author silva
 */
class abc  extends RADT_Controller{
    
    function __construct() {
        
        parent::__construct();
        $html=$this->view->fetch('abc');
        $this->load_html($html);
        $this->load_model('abc');        
    }
    
    
    public function index(){
        
        sel('status')->innertext="This is loaded from the controller.";
        
       // $x=User::create(array('username'=>'xyz','password'=>'ssss'));
        //$x->username='zzzzzz';
        //$x->save();
        $u=User::find(2);
        
        $u->username='222222';
        $u->save();
        
//$u->delete();
        
        $this->display_html();
    }
    
}