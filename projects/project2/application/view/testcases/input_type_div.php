<?php

/*
 * Rapid Application Development Framework
  Copyright (C) 2012  D'Silva Avinash Francis

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
?>
<html>
    <head>
        <title>input_type_div</title>
    </head>
    <body>
           <p id="status">hello from testcases/input_type_div</p>
     
        input type=div	<br/>
                        
          
    <div id="dv">
  THis is a test
</div>
    
        methods:<br/>
                
                <p>
                setHTMl <input type="button" id="setHTML" value="test" /><br/>	
		getHTMl<input type="button" id="getHTML" value="test" /><br/>	
                appendHTML<input type="button" id="apprendHTML" value="test" /><br/>
                hidden	true/false<input type="button" id="hidden_true" value="test true" />
                <input type="button" id="hidden_false" value="test false" /><br/>
    </body>
</html>